#!/bin/bash

# Synchronize files.
rsync -rvht --exclude '.DS_Store'\
 --exclude '.git'\
 --exclude '.gitignore'\
 --exclude 'deploy.*.sh'\
 ./ ~/Stages/test/source